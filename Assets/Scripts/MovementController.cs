﻿using UnityEngine;

public class MovementController : MonoBehaviour
{

    [SerializeField]
    private float movementSpeed = 1f;
    [SerializeField]
    private string horizontalAxis;
    [SerializeField]
    private string verticalAxis;
    
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis(horizontalAxis);
        float vertical = Input.GetAxis(verticalAxis);
        transform.position += new Vector3(horizontal, vertical) * Time.fixedDeltaTime * movementSpeed;
    }

}
