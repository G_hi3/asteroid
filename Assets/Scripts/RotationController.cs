﻿using UnityEngine;

public class RotationController : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 1f;
    [SerializeField]
    private string horizontalAxis;
    [SerializeField]
    private string verticalAxis;

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis(horizontalAxis);
        float vertical = Input.GetAxis(verticalAxis);
        if (horizontal != 0 && vertical != 0)
        {
            transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(horizontal, vertical) * 180 / Mathf.PI * rotationSpeed, Vector3.back);
        }
    }
}
