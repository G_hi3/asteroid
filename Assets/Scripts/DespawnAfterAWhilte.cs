﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnAfterAWhilte : MonoBehaviour
{
    public float lifetime = 1;
    [SerializeField]
    private float lifetimeSoFar = 0;

    void FixedUpdate()
    {
        if (lifetimeSoFar < lifetime)
        {
            lifetimeSoFar += Time.fixedDeltaTime;
        } else
        {
            Destroy(gameObject);
        }
    }
}
