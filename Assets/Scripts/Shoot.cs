﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField]
    private GameObject jet;
    [SerializeField]
    private GameObject pointer;
    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private float projectileSpeed = 1;
    [SerializeField]
    private float respawnRate = 1;
    [SerializeField]
    private string horizontalAxis;
    [SerializeField]
    private string verticalAxis;
    [SerializeField]
    private float respawnBuffer = 0;

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis(horizontalAxis);
        float verticalInput = Input.GetAxis(verticalAxis);
        if (horizontalInput != 0 || verticalInput != 0)
        {
            if (respawnBuffer >= respawnRate)
            {
                GameObject spawned = Instantiate(projectile);
                spawned.transform.position = pointer.transform.position;
                Rigidbody2D rigidbody = spawned.GetComponent<Rigidbody2D>();
                rigidbody.AddForce(new Vector2(verticalInput, -horizontalInput) * projectileSpeed);
            } else
            {
                respawnBuffer += Time.fixedDeltaTime;
            }
        } else if (respawnBuffer != 0)
        {
            respawnBuffer = 0;
        }
    }
}
