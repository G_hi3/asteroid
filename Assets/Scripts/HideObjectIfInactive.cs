﻿using UnityEngine;

public class HideObjectIfInactive : MonoBehaviour
{
    [SerializeField]
    private GameObject target;
    [SerializeField]
    private float timeOffset = 0.2f;
    [SerializeField]
    private string horizontalAxis;
    [SerializeField]
    private string verticalAxis;
    [SerializeField]
    private Color opaque = Color.white;
    [SerializeField]
    private Color transparent = Color.clear;
    [SerializeField]
    private float m_timer = 0;
    [SerializeField]
    private SpriteRenderer m_sprite;

    void Start()
    {
        m_sprite = target.GetComponent<SpriteRenderer>();
    }

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis(horizontalAxis);
        float verticalInput = Input.GetAxis(verticalAxis);
        if (horizontalInput == 0 && verticalInput == 0)
        {
            if (m_sprite.color == transparent)
            {
                m_timer += Time.fixedDeltaTime;
            } else if (m_timer > timeOffset)
            {
                m_sprite.color = transparent;
                m_timer = 0;
            }
        } else
        {
            if (m_sprite.color == opaque)
            {
                m_timer += Time.fixedDeltaTime;
            } else if (m_timer > timeOffset)
            {
                m_sprite.color = opaque;
                m_timer = 0;
            }
        }
    }
}
